# Initiation à GIT

Assurez-vous d'avoir git à disposition sur votre poste.
L'objectif de cet activité est de vous familiariser avec GIT, gestionnaire de version et partage de code.

## Exercice 1: Git en ligne de commande
* Au sein d'un terminal
* Créer un répertoire **mon-depot**, puis se rendre dans ce repertoire
* Transformer le en dépot git avec la commande **git init**.
* Créer un fichier README.md dans votre repertoire contenant le contenu suivant : "Version 1 du fichier README."
* Ajouter le fichier README à l’index de votre dépot git avec la commande **git add README**.
* Les changements enregistrés à l’index à l’aide de la commande git add sont mis en attente pour être enregistrés lors du prochain commit. Faire un premier commit avec la commande **git commit**. La commande ouvre un éditeur de fichier pour obtenir un description brève du commit. Entrer "Ajout d’un fichier README", puis quitter l'éditeur.
Si tout se passe bien, votre premier commit est enregistré dans votre dépot.
* Apporter une modification à votre fichier README, par exemple, remplacer ”Version 1” par ”Version 2”.
* Ajouter de nouveau les changements apportés avec la commande git add README et sauvegarder les changements en exécutant de nouveau la commande git commit.
* Visualisez l’état de votre dépot git avec la commande gitk



## Exercice 2: Synchroniser plusieurs dépots Git

* Revenir dans le repertoire parent (celui qui contient mon-depot) et créer une copie du dépot précédent avec la commande **git clone mon-depot depot-de-bob**.
* Se rendre dans **depot-de-bob** et lancer gitk, que constatez vous ?
* Faire un changement dans le fichier README de depot-de-bob. Enregistrer le changement avec git add README, et git commit. Relancer gitk. Quel est l’état de depot-de-bob ? de mon-depot ?
* Ajouter un nouveau fichier Test.php quelconque dans mon-depot, l’ajouter à l’index et le commiter. Retourner dans depot-bob et tapper la commande gitpull. Quitter l’éditeur sans modifier le message et tapper la commande gitk.
Que s’est-il passé ?



## Exercice 3 : Création d'un premier dépot sur GitLab

* GitLab offre des services de stockage de dépots git.
* Inscrivez vous sur Gitlab et créez votre premier dépôt.
* Une fois que vous avez créé le nouveau dépôt, copiez son url et connectez-le à mon-depot en exécutant les commandes suivantes **git remote add origin <url du depot>** et **git push origin master**
* Qu'avez-vous fait ?



## Exercice 4 : Découverte de GIT et GitLab.
Définir ce que fait les fonctionnalités suivantes : 
* git init
* git log
* git status
* git push
* git remote -v
* git add 
* git rm
* git diff



## Exercice 5 : Lecture et compréhension 
Lire et suivre la documentation suivante : [Git Basics](http://people.irisa.fr/Anthony.Baire/git/git-for-beginners-handout.pdf)

